#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
 
using namespace std;
 
int main()
{
    // Creating socket
    int listening = socket(AF_INET, SOCK_STREAM, 0);
    if (listening == -1)
    {
        cout << "Can't create a socket! Quitting" << endl;
        return -1;
    }
 
    // Binding the Ip address and port to the socket
    int port = 1111;
    string ipAddress = "0.0.0.0";

    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(port);
    inet_pton(AF_INET, ipAddress.c_str(), &hint.sin_addr);
 
    bind(listening, (sockaddr*)&hint, sizeof(hint));
 
    // Telling Winsock the socket is for listening
    listen(listening, SOMAXCONN);
 
    // Waiting for a connection
    sockaddr_in client;
    socklen_t clientSize = sizeof(client);
 
    int clientSocket = accept(listening, (sockaddr*)&client, &clientSize);
 
    char host[NI_MAXHOST];      // Client's remote name
    char service[NI_MAXSERV];   // Service (i.e. port) the client is connect on
 
    memset(host, 0, NI_MAXHOST); // same as memset(host, 0, NI_MAXHOST);
    memset(service, 0, NI_MAXSERV);
 
    if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
    {
        cout << host << " connected on port " << service << endl;
    }
    else
    {
        inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
        cout << host << " connected on port " << ntohs(client.sin_port) << endl;
    }
 
    // Closing listening socket
    close(listening);
 
    // While loop: accepting message from client
    unsigned char buf[4096];
 
    while (true)
    {
        memset(buf, 0, 4096);
 
        //Waiting for client to send data
        int bytesReceived = recv(clientSocket, buf, 4096, 0);
        if (bytesReceived == -1)
        {
            cout << "Error in recv(). Quitting" << endl;
            break;
        }
 
        else
        {
            //Displaying message,decoding bytes
            int result[bytesReceived/4] = {};
            int bytesize = 4;
            for (int n = 0; n < bytesReceived/bytesize; n++)
            {
                for(int i=bytesize;i>=0; i--)
                {
                    result[n] = (result[n] << 8) +buf[i+bytesize*n];
                }
                cout<< result[n] << "\n";
            }
            
            
        }
 
    }
    // Closing the socket
    close(clientSocket);
 
    return 0;
}