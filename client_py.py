#!/usr/bin/env python3
import socket
import struct
import random


#Defining port and IP number
port=1111
IP='0.0.0.0'   
#Creating socket
try:
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#Allocating port and IP number to the socket,connecting to server
    client.connect((IP, port))
except socket.error as e:
    print("\nNo connection\nError message:  {}".format (e))
    client.close()
send = 1
while send == 1:
    #Reciving message from server
    try:
        v = []
        for i in range(8):
            v.append(random.randint(-2147483648,2147483647))
            print(v[i])
        data = struct.pack('%si' % len(v), *v)
    except ValueError as e:
        print ("\nIncorrect data\nError message:  {}".format (e))
        client.close()
        break
    try:
        client.send(data)
    except NameError as e:
        print ("\nCould not send data \nError message:  {}".format (e))
        client.close()
        break
    except BrokenPipeError as e:
        print ("\nClient disconnected, could not send data \nError message:  {}".format (e))
        client.close()
        break
    print("Sent message: \n {} " .format(data))
    a = input("Press enter to send data")
    if a != 0:
        send = 1
#Closing connection
client.close()
print ("\nDisconnected from server")